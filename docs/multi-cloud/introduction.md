# Welcome to my Documentation

Explore my personal learning portal, where I publish detailed notes on a variety of technological stacks. Explore my diverse portfolio, which highlights my talents and accomplishments. This website provides substantial documentation, simple tutorials, and informative blog pieces to help you on your learning journey. Join me as I exhibit my portfolio and provide resources gleaned from my personal learning experiences and thoughts.

## Topics
- Multi-Cloud:
  - Amazon Web Services (AWS)
  - Microsoft Azure (Azure)
  - Google Cloud Platform (GCP)
- Kubernetes
- Docker
- Python
- Dotfiles
- Infraestructure as a code:
  - Terraform
  - Ansible
  - Obervability and Monitoring
- Sysadmin
  - Active Directory
  - Linux Administration
- Scripting
- Git
- Database
- Microservices (Nginx)
 
 ## Projects
 - Personal Project
 - Colaborations
 - Blog (IFTT)